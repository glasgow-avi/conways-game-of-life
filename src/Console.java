import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Console extends JFrame
{
	protected static JButton solveButton;
	protected static City city;

	protected static boolean solve = false;

	public Console()
	{
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		setLayout(new FlowLayout());
		city = new City();
		solveButton = new JButton("Solve");
		add(city);
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		panel.add(solveButton);
		add(panel);
		pack();

		solveButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				solve = true;
			}
		});
	}

	public static void main(String[] args)
	{
		new Console();
	}
}
