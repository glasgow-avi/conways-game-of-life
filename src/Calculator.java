import static java.lang.Thread.sleep;

public class Calculator extends Console
{
	private static int getNeighbours(int x, int y)
	{
		int neighbours = 0;
		try { neighbours += city.population[x - 1][y - 1].status ? 1 : 0;} catch (ArrayIndexOutOfBoundsException e){}
		try { neighbours += city.population[x - 1][y].status ? 1 : 0;} catch (ArrayIndexOutOfBoundsException e){}
		try { neighbours += city.population[x - 1][y + 1].status ? 1 : 0;} catch (ArrayIndexOutOfBoundsException e){}
		try { neighbours += city.population[x][y - 1].status ? 1 : 0;} catch (ArrayIndexOutOfBoundsException e){}
		try { neighbours += city.population[x][y + 1].status ? 1 : 0;} catch (ArrayIndexOutOfBoundsException e){}
		try { neighbours += city.population[x + 1][y - 1].status ? 1 : 0;} catch (ArrayIndexOutOfBoundsException e){}
		try { neighbours += city.population[x + 1][y].status ? 1 : 0;} catch (ArrayIndexOutOfBoundsException e){}
		try { neighbours += city.population[x + 1][y + 1].status ? 1 : 0;} catch (ArrayIndexOutOfBoundsException e){}

		return neighbours;
	}

	private static void traverse()
	{
		for(int y = 0; y < City.populationY; y++)
			for(int x = 0; x < City.populationX; x++)
			{
				int noOfNeighbours = getNeighbours(x, y);
				Person person = city.population[x][y];
				if(person.status)
					if(noOfNeighbours < 2 || noOfNeighbours > 3)
						person.toggle = true;
				if(!person.status)
					if(noOfNeighbours == 3)
						person.toggle = true;
			}
	}

	private static void toggle()
	{
		for(int y = 0; y < City.populationY; y++)
			for(int x = 0; x < City.populationX; x++)
			{
				Person person = city.population[x][y];
				if(person.toggle)
					person.toggle();
			}
	}

	public static void main(String[] args)
	{
		Calculator program = new Calculator();

		while(!program.solve)
		{
			System.out.print("");
		}

		System.out.print("Solving");

		while(true)
		{
			traverse();
			toggle();
			try
			{
				sleep(1000);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
}