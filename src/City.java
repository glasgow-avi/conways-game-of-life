import javax.swing.*;
import java.awt.*;

public class City extends JPanel
{
	public static final int populationX = 30, populationY = 20;
	public Person[][] population = new Person[populationX][populationY];

	public City()
	{
		setLayout(new GridLayout(populationY, populationX));

		for(int y = 0; y < populationY; y++)
			for(int x = 0; x < populationX; x++)
			{
				population[x][y] = new Person();
				add(population[x][y]);
			}
	}
}
