import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Person extends JButton
{
	public boolean status;
	public boolean toggle;

	public void toggle()
	{
		if(status)
			kill();
		else birth();
		System.out.println(status ? "Alive" : "Dead");
	}

	public void birth()
	{
		toggle = false;
		status = true;
		setBackground(Color.black);
	}

	public void kill()
	{
		toggle = false;
		status = false;
		setBackground(Color.white);
	}

	public Person()
	{
		setBackground(Color.white);
		setPreferredSize(new Dimension(30, 30));

		addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				toggle();
			}
		});
	}
}